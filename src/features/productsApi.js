import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const productsApi = createApi({
  reducerPath: "productsApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://capstone-2-codera.onrender.com",
  }),
  endpoints: (builder) => ({
    getAllProducts: builder.query({
      query: () => "api/products",
    }),
  }),
});

export const { useGetAllProductsQuery } = productsApi;
