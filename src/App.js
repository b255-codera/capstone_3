import "./App.css";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Layout from "./components/Layout";
import Collection from "./pages/Collection";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Account from "./pages/Account";
import MenPage from "./pages/ForMen";
import WomenPage from "./pages/ForWomen";
import ProductPage from "./pages/ProductPage";
import Cart from "./pages/Cart";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext";
import AOS from "aos";
import "aos/dist/aos.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CheckoutSuccess from "./pages/CheckoutSuccess";

function App() {
  AOS.init();
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log("User1:", user);
    console.log("LocalStorage", localStorage);
  }, [user]);
  const token = localStorage.getItem("token");

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <BrowserRouter>
          <ToastContainer />
          <Routes>
            <Route path="/" exact element={<Layout />}>
              <Route index element={<Home />} />
              <Route
                path="/account/login"
                element={token ? <Navigate to="/account" /> : <Login />}
              />
              <Route
                path="/account/register"
                element={token ? <Navigate to="/account" /> : <Register />}
              />
              <Route
                path="/account"
                element={
                  !token ? <Navigate to="/account/login" /> : <Account />
                }
              />
              <Route path="/collections/all" element={<Collection />} />
              <Route
                path="/collections/fragrance-for-men"
                element={<MenPage />}
              />
              <Route
                path="/collections/fragrance-for-women"
                element={<WomenPage />}
              />
              <Route path="/product/:productId" element={<ProductPage />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/checkout-success" element={<CheckoutSuccess />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </UserProvider>
    </>
  );
}

export default App;
