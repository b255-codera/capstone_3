import React from "react";
import { useState, useEffect } from "react";
import { generatePublicUrl } from "../urlConfig";
import { Link, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addToCart } from "../features/cartSlice";

const ProductPage = () => {
  const { productId } = useParams();
  const dispatch = useDispatch();
  const [imageUrls, setImageUrls] = useState([]);
  const [product, setProduct] = useState([]);
  const [value, setValue] = useState(1);

  const handleChange = (event) => {
    let inputValue = parseInt(event.target.value, 10);
    if (inputValue < 1) {
      inputValue = 1;
    }

    setValue(inputValue);
  };

  const handleAddToCart = (product) => {
    dispatch(addToCart(product));
  };

  useEffect(() => {
    getProduct();
  });

  const imageClick = (newImageUrl) => {
    const mainImage = document.querySelector(".image-1");
    mainImage.src = newImageUrl;
  };

  const getProduct = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/api/products/find/${productId}`,
        {
          method: "GET",
        }
      );

      const data = await response.json();
      const productData = data.product;
      setProduct(productData);
      setImageUrls(
        productData.productImages.map((image) => generatePublicUrl(image.img))
      );
      console.log("Hey", imageUrls);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <div className="container product my-5 pt-5">
        <div className="row mt-5">
          <div className="col-lg-5 col-md-12 col-12">
            <img
              src={imageUrls[0]}
              alt=""
              className="img-fluid w-100 pb-1 image-1"
            />
            <div className="small-img-group d-flex justify-content-start">
              {imageUrls.map((url, index) => (
                <div className="small-img-col" key={index}>
                  <img
                    src={url}
                    alt={url}
                    className="small-img"
                    style={{ width: "100%" }}
                    onClick={() => imageClick(url)}
                  />
                </div>
              ))}
            </div>
          </div>
          <div className="col-lg-6 col-md-12 col-12">
            <h3 className="py-4">{product.name}</h3>
            <h2>
              &#8369;{Math.round(product.price - product.price * 0.2)}
              <strike className="ms-3 fs-4 text-muted">
                &#8369;{product.price}
              </strike>
            </h2>
            <input
              type="number"
              value={value}
              min="1"
              max={product.quantity}
              onChange={handleChange}
            />
            <Link to="/cart" className="text-dark text-decoration-none">
              <button
                className="buy-btn"
                onClick={() => handleAddToCart(product)}
              >
                Add To Cart
              </button>
            </Link>
            <h5 className="mt-4">Availability: In stock</h5>
            <h4 className="mt-5 mb-4">Product Description</h4>
            <span>{product.description}</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProductPage;
