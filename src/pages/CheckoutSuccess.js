import React from "react";
import { Link } from "react-router-dom";

const CheckoutSuccess = () => {
  return (
    <>
      <div className="div-area">
        <div className="bg-overlay">
          <div className="container">
            <Link
              to="/"
              title="Go back to the homepage"
              className="text-decoration-none fs-5 text-light d-inline-block mx-3"
            >
              Home
            </Link>
            <span className="fs-5 text-light">/</span>
            <span className="fs-5 text-light mx-3">Success</span>
          </div>
        </div>
      </div>
      <div className="clear"></div>
      <h2 className="py-5 my-5 text-center">
        Checkout Successful. Thanks for ordering!
      </h2>
      <br />
      <br />
      <br />
      <br />
      <br />
    </>
  );
};

export default CheckoutSuccess;
