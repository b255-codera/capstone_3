import React, { useEffect, useState } from "react";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

const Account = () => {
  const { setUser } = useContext(UserContext);
  const navigate = useNavigate();
  const [userDetails, setUserDetails] = useState({
    firstName: "",
    lastName: "",
    email: "",
  });

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (!token) {
      navigate("/account/login");
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setUserDetails(data.user);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [navigate, userDetails]);

  const logoutUser = () => {
    setUser({ id: null });
    localStorage.removeItem("token");
    navigate("/account/login");
  };

  return (
    <>
      <div className="div-area">
        <div className="bg-overlay">
          <div className="container">
            <Link
              to="/"
              title="Go back to the homepage"
              className="text-decoration-none fs-5 text-light d-inline-block mx-3"
            >
              Home
            </Link>
            <span className="fs-5 text-light">/</span>
            <span className="fs-5 text-light mx-3">Account</span>
          </div>
        </div>
      </div>
      <div className="clear"></div>
      <section className="account-details">
        <div className="container">
          <div className="row outer-row py-3 mt-5">
            <div className="col-6 px-0">
              <h1>My Account</h1>
            </div>
            <div className="col-6 d-flex justify-content-end px-0">
              <button className="logoutBtn" onClick={logoutUser}>
                Logout
              </button>
            </div>
          </div>
          <div className="row py-3">
            <div className="col-lg-8 px-0 order-details">
              <h3>Order History</h3>
              <p className="mt-4">You haven't placed any order yet.</p>
            </div>
            <div className="col-lg-4 px-0 account-details">
              <h3>Account Details</h3>
              <p className="mt-4">
                {userDetails.firstName} {userDetails.lastName}
              </p>
              <p>{userDetails.email}</p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Account;
