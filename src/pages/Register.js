import React from "react";
import { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import UserContext from "../UserContext";

const Register = () => {
  const { user } = useContext(UserContext);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const navigate = useNavigate();

  const showToastMessage = () => {
    toast.success("Account registration successful!", {
      position: toast.POSITION.TOP_RIGHT,
    });
  };

  const clearFields = () => {
    setFirstName("");
    setLastName("");
    setEmail("");
    setPassword("");
  };
  const registerUser = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/api/users/signup`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            firstName,
            lastName,
            email,
            password,
          }),
        }
      );

      if (response.status === 409) {
        const data = await response.json();
        setErrorMessage(data.message);
        clearFields();
      } else if (response.status === 422) {
        const data = await response.json();
        setErrorMessage(data.message);
      } else if (response.status === 500) {
        const data = await response.json();
        setErrorMessage(data.message);
        clearFields();
      } else if (response.status === 201) {
        showToastMessage();
        clearFields();
        setErrorMessage("");
        navigate("/account/login");
      }
    } catch (error) {
      console.error(error);
      setErrorMessage("An unexpected error occurred.");
    }
  };

  return (
    <>
      <div className="div-area">
        <div className="bg-overlay">
          <div className="container">
            <Link
              to="/"
              title="Go back to the homepage"
              className="text-decoration-none fs-5 text-light d-inline-block mx-3"
            >
              Home
            </Link>
            <span className="fs-5 text-light">/</span>
            <span className="fs-5 text-light mx-3">Create Account</span>
          </div>
        </div>
      </div>
      <div className="clear"></div>
      <div className="formContainer mt-5">
        <div className="container">
          <div className="user-account">
            <div className="grid-item">
              <div className="register-form">
                <form className="form-main" onSubmit={registerUser}>
                  <h4 class="text-center">Create my account</h4>
                  <p class="text-center mb-4">
                    Please fill in the information below:
                  </p>
                  {errorMessage && (
                    <p className="errorMessage">{errorMessage}</p>
                  )}
                  <label className="d-none" for="firstName">
                    First Name
                  </label>
                  <input
                    type="text"
                    id="firstName"
                    placeholder="First Name"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                  <label className="d-none" for="lastName">
                    Last Name
                  </label>
                  <input
                    type="text"
                    id="lastName"
                    placeholder="Last Name"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                  <label className="d-none" for="email">
                    Email
                  </label>
                  <input
                    type="email"
                    id="email"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <label className="d-none" for="password">
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <p>
                    <button type="submit">Submit</button>
                  </p>
                  <p>
                    <Link to="/" className="text-decoration-none text-dark">
                      Return to Home
                    </Link>
                  </p>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Register;
