import React from "react";
import { Link } from "react-router-dom";
import StarIcon from "@mui/icons-material/Star";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { useState, useEffect } from "react";
import { generatePublicUrl } from "../urlConfig";
import { useDispatch } from "react-redux";
import { addToCart } from "../features/cartSlice";

const ForWomen = () => {
  const [womenProducts, setWomenProducts] = useState([]);
  const dispatch = useDispatch();

  const handleAddToCart = (product) => {
    dispatch(addToCart(product));
  };

  useEffect(() => {
    getWomenProducts();
  }, []);

  const getWomenProducts = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/api/products/active?category=women`,
        {
          method: "GET",
        }
      );

      const data = await response.json();

      setWomenProducts(data.products);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <div className="div-area">
        <div className="bg-overlay">
          <div className="container">
            <Link
              to="/"
              title="Go back to the homepage"
              className="text-decoration-none fs-5 text-light d-inline-block mx-3"
            >
              Home
            </Link>
            <span className="fs-5 text-light">/</span>
            <span className="fs-5 text-light mx-3">For Her</span>
          </div>
        </div>
      </div>
      <div className="clear"></div>
      <div className="container" id="product-cards">
        <h1 className="text-center fw-bold">Women's Collection</h1>
        <div className="row py-5" style={{ marginTop: "30px" }}>
          {womenProducts.map((product, index) => (
            <div className="col-md-3 py-3">
              <div className="card">
                <Link
                  to={`/product/${product._id}`}
                  className="text-decoration-none"
                >
                  <div className="img-wrapper">
                    <img
                      src={generatePublicUrl(product.productImages[0].img)}
                      alt={product.name}
                    />
                  </div>
                </Link>
                <div className="card-body">
                  <Link
                    to={`/product/${product._id}`}
                    className="text-decoration-none text-dark"
                  >
                    <h3>{product.name}</h3>
                  </Link>
                  <div className="star">
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                  </div>
                  <h5>
                    &#8369;{Math.round(product.price - product.price * 0.2)}
                    <strike>&#8369;{product.price}</strike>
                    <Link to="/cart" className="text-dark text-decoration-none">
                      <span onClick={() => handleAddToCart(product)}>
                        <ShoppingCartIcon />
                      </span>
                    </Link>
                  </h5>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default ForWomen;
