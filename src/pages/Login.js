import React from "react";
import { useState, useContext } from "react";
import UserContext from "../UserContext";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";

const Login = (props) => {
  const navigate = useNavigate();
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const showToastMessage = () => {
    toast.success("Success!", {
      position: toast.POSITION.TOP_RIGHT,
    });
  };

  const clearFields = () => {
    setEmail("");
    setPassword("");
  };
  const authenticate = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/api/users/login`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: email,
            password: password,
          }),
        }
      );
      const data = await response.json();

      if (typeof data.accessToken !== "undefined" && response.status === 200) {
        localStorage.setItem("token", data.accessToken);
        await retrieveUserDetails(data.accessToken);
        showToastMessage();
        clearFields();
        setErrorMessage("");
        navigate("/");
      } else if (response.status === 422) {
        setErrorMessage(data.message);
      } else if (response.status === 400) {
        setErrorMessage(data.message);
        clearFields();
      } else if (response.status === 500) {
        setErrorMessage(data.message);
        clearFields();
      }
    } catch (error) {
      console.error(error);
      setErrorMessage("An unexpected error occurred.");
    }
  };

  const retrieveUserDetails = async (token) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/api/users/details`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      const data = await response.json();

      setUser({
        id: data.user._id,
        isAdmin: data.user.isAdmin,
      });
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <div className="div-area">
        <div className="bg-overlay">
          <div className="container">
            <Link
              to="/"
              title="Go back to the homepage"
              className="text-decoration-none fs-5 text-light d-inline-block mx-3"
            >
              Home
            </Link>
            <span className="fs-5 text-light">/</span>
            <span className="fs-5 text-light mx-3">Login</span>
          </div>
        </div>
      </div>
      <div className="clear"></div>
      <div className="loginPage">
        <div className="container">
          <div className="row mt-5">
            <div className="col-12">
              <div className="text-center">
                <h4>Login</h4>
                <p>Please login using your account details below:</p>
              </div>
            </div>
          </div>
          <div className="row justify-content-center align-items-center">
            <div className="col-lg-6 loginForm">
              <form className="row mx-0 mb-3" onSubmit={authenticate}>
                {errorMessage && <p className="errorMessage">{errorMessage}</p>}
                <label className="d-none" for="email">
                  Email
                </label>
                <input
                  type="email"
                  id="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <label className="d-none" for="password">
                  Password
                </label>
                <input
                  type="password"
                  id="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <p className="px-0">
                  <button type="submit">Sign In</button>
                </p>
              </form>
              <p>
                <Link
                  to="/account/register"
                  className="text-decoration-none text-dark"
                >
                  Create Account
                </Link>
              </p>
              <p>
                <Link to="/" className="text-decoration-none text-dark">
                  Return to Home
                </Link>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
