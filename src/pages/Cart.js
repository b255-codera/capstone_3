import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { generatePublicUrl } from "../urlConfig";
import {
  addToCart,
  clearCart,
  decreaseCart,
  getTotal,
  removeFromCart,
} from "../features/cartSlice";
import PayButton from "../components/PayButton";

const Cart = () => {
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getTotal());
  }, [cart, dispatch]);

  const handleRemoveFromCart = (cartItem) => {
    dispatch(removeFromCart(cartItem));
  };

  const handleDecreaseCart = (cartItem) => {
    dispatch(decreaseCart(cartItem));
  };

  const handleIncreaseCart = (cartItem) => {
    dispatch(addToCart(cartItem));
  };

  const handleClearCart = () => {
    dispatch(clearCart());
  };

  const token = localStorage.getItem("token");
  return (
    <>
      <div className="div-area">
        <div className="bg-overlay">
          <div className="container">
            <Link
              to="/"
              title="Go back to the homepage"
              className="text-decoration-none fs-5 text-light d-inline-block mx-3"
            >
              Home
            </Link>
            <span className="fs-5 text-light">/</span>
            <span className="fs-5 text-light mx-3">Cart</span>
          </div>
        </div>
      </div>
      <div className="clear"></div>
      <div className="cart-container">
        <h2>Shopping Cart</h2>
        {cart.cartItems.length === 0 ? (
          <div className="cart-empty pb-5 mb-5">
            <p>Your cart is currently empty</p>
            <div className="start-shopping pb-5">
              <Link to="/collections/all">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="20"
                  height="20"
                  fill="currentColor"
                  className="bi bi-arrow-left"
                  viewBox="0 0 16 16"
                >
                  <path
                    fillRule="evenodd"
                    d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
                  />
                </svg>
                <span>Start Shopping</span>
              </Link>
            </div>
          </div>
        ) : (
          <div>
            <div className="titles">
              <h3 className="product-title">Product</h3>
              {/* <h3 className="price">Price</h3> */}
              <h3 className="quantity">Quantity</h3>
              <h3 className="subtotal">Subtotal</h3>
            </div>
            <div className="cart-items">
              {cart.cartItems?.map((cartItem) => (
                <div className="cart-item" key={cartItem._id}>
                  <div className="cart-product">
                    <img
                      src={generatePublicUrl(cartItem.productImages[0].img)}
                      alt=""
                    />
                    <div>
                      <h3>{cartItem.name}</h3>
                      <p>
                        &#8369;
                        {Math.round(cartItem.price - cartItem.price * 0.2)}
                      </p>
                      <button onClick={() => handleRemoveFromCart(cartItem)}>
                        Remove
                      </button>
                    </div>
                  </div>
                  {/* <div className="cart-product-price">P3000</div> */}
                  <div className="cart-product-quantity">
                    <button onClick={() => handleDecreaseCart(cartItem)}>
                      -
                    </button>
                    <div className="count">{cartItem.cartQuantity}</div>
                    <button onClick={() => handleIncreaseCart(cartItem)}>
                      +
                    </button>
                  </div>
                  <div className="cart-product-subtotal-price">
                    &#8369;
                    {Math.round(cartItem.price - cartItem.price * 0.2) *
                      cartItem.cartQuantity}
                  </div>
                </div>
              ))}
            </div>
            <div className="cart-summary">
              <button className="clear-cart" onClick={() => handleClearCart()}>
                Reset Cart
              </button>
              <div className="cart-checkout">
                <div className="total">
                  <span>Total</span>
                  <span className="amount">
                    &#8369;
                    {Math.round(
                      cart.cartTotalAmount - cart.cartTotalAmount * 0.2
                    )}
                  </span>
                </div>
                <p>Free Shipping</p>
                {token ? (
                  <PayButton cartItems={cart.cartItems} />
                ) : (
                  <Link to="/account/login">
                    <button className="cart-login">Login to Check out</button>
                  </Link>
                )}

                <div className="continue-shopping">
                  <Link to="/collections/all">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="20"
                      height="20"
                      fill="currentColor"
                      className="bi bi-arrow-left"
                      viewBox="0 0 16 16"
                    >
                      <path
                        fillRule="evenodd"
                        d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
                      />
                    </svg>
                    <span>Continue Shopping</span>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Cart;
