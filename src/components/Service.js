import React from "react";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import VolunteerActivismOutlinedIcon from "@mui/icons-material/VolunteerActivismOutlined";
import WorkspacePremiumOutlinedIcon from "@mui/icons-material/WorkspacePremiumOutlined";

const Service = () => {
  return (
    <>
      <div className="service py-5">
        <div className="container text-center py-5">
          <div
            className="row pb-5"
            data-aos="fade-right"
            data-aos-delay="300"
            data-aos-duration="3000"
          >
            <div className="col-lg-4">
              <LocalShippingOutlinedIcon />
              <h3 className="py-3">Free Shipping</h3>
              <p>
                Orders ship on the day that you place them and arrive within
                days.
              </p>
            </div>
            <div className="col-lg-4">
              <VolunteerActivismOutlinedIcon />
              <h3 className="py-3">Money back guarantee</h3>
              <p>
                Don't like the item? We offer full refund and money back
                guarantee within 30 days.
              </p>
            </div>
            <div className="col-lg-4">
              <WorkspacePremiumOutlinedIcon />
              <h3 className="py-3">Genuine Products</h3>
              <p>
                100% authentic fragrances. You won't find knockoffs or
                imitations here.
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Service;
