import React from "react";
import { Link } from "react-router-dom";
const Banner = () => {
  return (
    <>
      <div className="banner d-flex justify-content-center align-items-center">
        <div
          className="banner-content"
          data-aos="zoom-in"
          data-aos-delay="50"
          data-aos-duration="3000"
        >
          <h3>Best Deal of the Week</h3>
          <p>
            Buy 3 items and get extra <span>10% Off</span> on top of existing
            discounted prices.
          </p>
          <button>
            <Link to="/collections/all">Shop Now</Link>
          </button>
        </div>
      </div>
    </>
  );
};

export default Banner;
