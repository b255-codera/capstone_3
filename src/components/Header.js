import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <>
      <div
        id="carouselExampleIndicators"
        className="carousel slide"
        data-bs-ride="carousel"
      >
        <div className="carousel-indicators">
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="0"
            className="active"
            aria-current="true"
            aria-label="Slide 1"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="1"
            aria-label="Slide 2"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="2"
            aria-label="Slide 3"
          ></button>
        </div>
        <div className="carousel-inner">
          <div className="carousel-item active myslider fadeSlider">
            <div className="txt">
              <h1 className="mx-0">Up to 30% OFF</h1>
              <p>All items are on sale! Limited time offer.</p>
              <button className="homebtn">
                <Link
                  to="/collections/all"
                  className="text-decoration-none text-white"
                >
                  Shop Now
                </Link>
              </button>
            </div>
            <img src="/img/header1.jpg" className="d-block w-100" alt="..." />
          </div>
          <div className="carousel-item myslider fadeSlider">
            <div className="txt">
              <h1 className="mx-0">Free Shipping</h1>
              <p>Free shipping and delivery nationwide!</p>
              <button className="homebtn">
                <Link
                  to="/collections/all"
                  className="text-decoration-none text-white"
                >
                  Shop Now
                </Link>
              </button>
            </div>
            <img src="/img/header2.jpg" className="d-block w-100" alt="..." />
          </div>
          <div className="carousel-item myslider fadeSlider">
            <div className="txt">
              <h1 className="mx-0">Great Quality</h1>
              <p>We guarantee all our perfumes are original and authentic!</p>
              <button className="homebtn">
                <Link
                  to="/collections/all"
                  className="text-decoration-none text-white"
                >
                  Shop Now
                </Link>
              </button>
            </div>
            <img src="/img/header3.jpg" className="d-block w-100" alt="..." />
          </div>
        </div>
        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target="#carouselExampleIndicators"
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target="#carouselExampleIndicators"
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    </>
  );
};

export default Header;
