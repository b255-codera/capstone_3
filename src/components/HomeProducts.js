import React from "react";
import { Link } from "react-router-dom";
import StarIcon from "@mui/icons-material/Star";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { useState, useEffect } from "react";
import { generatePublicUrl } from "../urlConfig";
import { useDispatch } from "react-redux";
import { addToCart } from "../features/cartSlice";

const HomeProducts = () => {
  const [latestProducts, setLatestProducts] = useState([]);
  const dispatch = useDispatch();

  const handleAddToCart = (product) => {
    dispatch(addToCart(product));
  };

  useEffect(() => {
    getLatestProducts();
  }, []);

  const getLatestProducts = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/api/products/latest`,
        {
          method: "GET",
        }
      );

      const data = await response.json();

      setLatestProducts(data.latestProducts);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <div className="container" id="product-cards">
        <h1 className="text-center fw-bold">Latest Products</h1>
        <div className="row" style={{ marginTop: "30px" }}>
          {latestProducts.map((product, index) => (
            <div className="col-md-3 py-3">
              <div className="card">
                <Link
                  to={`/product/${product._id}`}
                  className="text-decoration-none"
                >
                  <div className="img-wrapper">
                    <img
                      src={generatePublicUrl(product.productImages[0].img)}
                      alt=""
                    />
                  </div>
                </Link>
                <div className="card-body">
                  <Link
                    to={`/product/${product._id}`}
                    className="text-decoration-none text-dark"
                  >
                    <h3>{product.name}</h3>
                  </Link>
                  <div className="star">
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                  </div>
                  <h5>
                    &#8369;{Math.round(product.price - product.price * 0.2)}
                    <strike>&#8369;{product.price}</strike>
                    <Link to="/cart" className="text-dark text-decoration-none">
                      <span onClick={() => handleAddToCart(product)}>
                        <ShoppingCartIcon />
                      </span>
                    </Link>
                  </h5>
                </div>
              </div>
            </div>
          ))}
        </div>

        <Link
          to="/collections/all"
          id="viewmorebtn"
          className="d-flex justify-content-center text-decoration-none"
        >
          View All
        </Link>
      </div>
    </>
  );
};

export default HomeProducts;
