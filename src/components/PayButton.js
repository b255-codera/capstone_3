import axios from "axios";
import jwtDecode from "jwt-decode";

const PayButton = ({ cartItems }) => {
  const token = localStorage.getItem("token");

  const handleCheckout = () => {
    if (token) {
      const decodedToken = jwtDecode(token);
      const userId = decodedToken.id;
      axios
        .post(
          `${process.env.REACT_APP_API_URL}/api/stripe/create-checkout-session`,
          {
            cartItems,
            userId: userId,
          }
        )
        .then((res) => {
          if (res.data.url) {
            window.location.href = res.data.url;
          }
        })
        .catch((error) => console.log(error.message));
    } else {
      console.log("Token not found.");
    }
  };

  return (
    <>
      <button onClick={() => handleCheckout()}>Check Out</button>
    </>
  );
};

export default PayButton;
