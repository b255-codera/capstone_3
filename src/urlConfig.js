export const generatePublicUrl = (filename) => {
  return `https://capstone-2-codera.onrender.com/uploads/${filename}`;
};
